/*
** Zabbix
** Copyright (C) 2001-2018 Zabbix SIA
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, version 3.
**
** This program is distributed in the hope that it will be useful, but
** WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

package main

import (
	"strconv"
	"strings"

	"github.com/vadimipatov/go-zabbix"
	"gopkg.in/errgo.v1"
)

var TriggerParamsList = []string{
	"triggerid",
	"expression",
	"description",
	"priority",
	"state",
	"status",
	"value",
	"lastchange",
	"hostname",
}

var ProblemTriggerParamsList = map[int]string{
	2:  "triggerid",
	4:  "description",
	5:  "priority",
	9:  "lastchange",
	10: "hostname",
}

const (
	_ = iota
	TP_OIDSUFFIX
	TP_ID
	TP_EXPRESSION
	TP_DESCRIPTION
	TP_SEVERITY
	TP_STATE
	TP_ENABLED
	TP_ALARMSTATE
	TP_LASTCHANGE
	TP_HOSTNAME
)

// getTrigger
func getTrigger(triggerID string) (*zabbix.Trigger, error) {
	response, err := zbxapi.GetTriggers(zabbix.TriggerGetParams{
		TriggerIDs:        []string{triggerID},
		SelectTags:        "extend",
		ExpandDescription: true,
		SelectHosts:       []string{"host"},
		ExpandExpression:  true,
		GetParameters:     zabbix.GetParameters{ResultLimit: 1},
	})

	if err != nil {
		return nil, err
	}

	// TODO: check exist
	if len(response) == 0 {
		return nil, errgo.Newf("trigger '%s' not found", triggerID)
	}
	return &response[0], nil
}

// Trigger handlers fabric
func createTriggerHandler(triggerID string, param string) func(transactionID string) interface{} {
	return func(transactionID string) interface{} {
		var (
			trigger *zabbix.Trigger
			err     error
		)

		result := transactionCache.Get(transactionID, triggerID)
		if result != nil {
			dprintf("<- transactionCache [t:%s,p:%s]", triggerID, param)
			trigger = result.(*zabbix.Trigger)
		} else {
			result = queryCache.Get(triggerID)
			if result != nil {
				dprintf("<- queryCache [t:%s,p:%s]", triggerID, param)
				trigger = result.(*zabbix.Trigger)
				transactionCache.Push(transactionID, triggerID, trigger)
			} else {
				trigger, err = getTrigger(triggerID)
				dprintf("<- API [t:%s,p:%s]", triggerID, param)
				if err != nil {
					return errgo.Newf("error while getting trigger '%s' data from api: %s", triggerID, err)
				}
				transactionCache.Push(transactionID, triggerID, trigger)
				queryCache.Push(triggerID, trigger)
			}
		}

		if trigger == nil {
			return errgo.Newf("error while getting '%s'", triggerID)
		}

		switch param {
		case "triggerid":
			v, err := strconv.Atoi(trigger.TriggerID)
			if err != nil {
				return err
			}
			return int32(v)
		case "expression":
			return trigger.Expression
		case "description":
			return trigger.Description
		case "priority":
			return int32(trigger.Severity)
		case "state":
			return int32(trigger.State)
		case "status":
			if trigger.Enabled {
				return int32(1)
			}
			return int32(0)
		case "value":
			return int32(trigger.AlarmState)
		case "lastchange":
			return int32(trigger.LastChange)
		case "hostname":
			return strings.TrimSuffix(getHostnamesString(trigger), ",")
		default:
			return errgo.Newf("unknown param '%s'", param)
		}
	}
}

func getHostnamesString(trigger *zabbix.Trigger) (hostnames string) {
	if trigger == nil {
		return
	}

	for _, host := range trigger.Hosts {
		hostnames += host.Hostname + ","
	}

	return strings.TrimSuffix(hostnames, ",")
}
