package main

import (
	"log"
	"os"
)

// debug caches the value of environment variable SNMPGW_DEBUG_HANDLERS from program start.
var debugHandlers bool = (os.Getenv("SNMPGW_DEBUG_HANDLERS") == "1")

// dprintf prints formatted debug message to STDOUT if the SNMPGW_DEBUG_HANDLERS environment
// variable is set to "1".
func dprintf(format string, a ...interface{}) {
	if debugHandlers {
		log.Printf(format, a...)
	}
}
