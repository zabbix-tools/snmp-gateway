/*
** Zabbix
** Copyright (C) 2001-2018 Zabbix SIA
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, version 3.
**
** This program is distributed in the hope that it will be useful, but
** WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

package main

import (
	"fmt"
	"log"
	"reflect"
	"sort"
	"strconv"
	"strings"

	"github.com/vadimipatov/go-agentx/pdu"
	"github.com/vadimipatov/go-agentx/value"
)

// ListItem defines an item of the list handler.
type ListItem struct {
	Type    pdu.VariableType
	Handler func(transactionID string) interface{}
}

type AgentXHandler struct {
	oids  sort.StringSlice
	items map[string]*ListItem
}

// Get tries to find the provided oid and returns the corresponding value.
func (this *AgentXHandler) Get(oid value.OID, header *pdu.Header) (value.OID, pdu.VariableType, interface{}, error) {
	var itemType pdu.VariableType

	if this.items == nil {
		return nil, pdu.VariableTypeNoSuchObject, nil, nil
	}

	// TODO: pass errors to result
	item, ok := this.items[oid.String()]
	if ok {
		value := item.Handler(fmt.Sprintf("%d.%d", header.SessionID, header.TransactionID))
		if value == nil {
			return oid, pdu.VariableTypeNoSuchObject, nil, nil
		}
		if strings.Contains(reflect.TypeOf(value).String(), "*err") {
			log.Println(value)
		}
		switch reflect.TypeOf(value).String() {
		case "int32":
			itemType = pdu.VariableTypeInteger
		case "string":
			itemType = pdu.VariableTypeOctetString
		default:
			return nil, pdu.VariableTypeNoSuchObject, nil, nil
		}
		return oid, itemType, value, nil
	}
	return nil, pdu.VariableTypeNoSuchObject, nil, nil
}

// GetNext tries to find the value that follows the provided oid and returns it.
func (this *AgentXHandler) GetNext(from value.OID, includeFrom bool, to value.OID, header *pdu.Header) (value.OID, pdu.VariableType, interface{}, error) {
	if this.items == nil {
		return nil, pdu.VariableTypeNoSuchObject, nil, nil
	}

	fromOID, toOID := from.String(), to.String()
	for _, oid := range this.oids {
		if oidWithin(oid, fromOID, includeFrom, toOID) {
			return this.Get(value.MustParseOID(oid), header)
		}
	}

	return nil, pdu.VariableTypeNoSuchObject, nil, nil
}

// Compare oid's nodes one by one as numeric values
func oidsCompare(a, b string) int {
	var (
		anode, bnode int
		err          error
	)

	if a == b {
		return 0
	}

	an := strings.Split(a, ".")
	bn := strings.Split(b, ".")

	for i, v := range bn {
		if i == len(an) || anode < bnode {
			break
		}
		anode, err = strconv.Atoi(an[i])
		if err != nil {
			panic("Invalid OID: " + a)
		}
		bnode, err = strconv.Atoi(v)
		if err != nil {
			panic("Invalid OID: " + b)
		}
		if anode > bnode {
			return 1
		}
	}

	return -1
}

func oidWithin(oid string, from string, includeFrom bool, to string) bool {
	fromCompare := oidsCompare(from, oid)
	toCompare := oidsCompare(to, oid)

	return (fromCompare == -1 || (fromCompare == 0 && includeFrom)) && (toCompare == 1)
}

func (this *AgentXHandler) Register(oid string, handler func(transactionID string) interface{}) error {
	if this.items == nil {
		this.items = make(map[string]*ListItem)
	}

	this.oids = append(this.oids, oid)

	// Natural sorting of oids (in numeric order)
	// Right order should be: 1,2,3,..,10 (not 1,10,2,3,..)
	sort.Slice(this.oids, func(i, j int) bool {
		if oidsCompare(this.oids[i], this.oids[j]) > 0 {
			return false
		} else {
			return true
		}
	})

	item := &ListItem{}
	item.Handler = handler
	this.items[oid] = item

	return nil
}

func (this *AgentXHandler) Unregister(oid string) {
	delete(this.items, oid)
}
