package main

import (
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"sync"
	"time"

	"github.com/Tomasen/realip"
	"github.com/vadimipatov/go-agentx"
	"github.com/vadimipatov/go-agentx/pdu"
	"github.com/vadimipatov/go-agentx/value"
	"github.com/vadimipatov/go-zabbix"
	"gopkg.in/errgo.v1"
)

var triggerMux sync.Mutex
var triggerExecution chan *execution
var queue []*execution
var executeRunning bool
var lastExecution time.Time

var queuedExecution bool

type execution struct {
	trigger       *zabbix.Trigger
	oid           string
	ip            string
	triggerID     string
	triggerID_int int
}

func randSession(sessions map[string]*agentx.Session) *agentx.Session {
	i := rand.Intn(len(sessions))
	for _, s := range sessions {
		if i == 0 {
			return s
		}
		i--
	}
	return sessions[defaultGroup] // impossible, nevertheless...
}

func trapHandler(w http.ResponseWriter, req *http.Request) {
	// TODO: stat: success/failed requests count
	var (
		trigger *zabbix.Trigger
		oid     string
	)

	err := req.ParseForm()
	if err != nil {
		log.Printf("Error while handling POST request [u:%s,f:%s]: %s\n", req.RequestURI, realip.FromRequest(req), err)
		w.WriteHeader(400)
		return
	}

	triggerID := req.Form.Get("triggerid")
	triggerID_int, err := strconv.Atoi(triggerID)
	if err != nil {
		log.Printf("Error while handling trap [f:%s]: wrong triggerid value: \"%s\"\n", realip.FromRequest(req), triggerID)
		w.WriteHeader(400)
		return
	}

	oid, err = mapTable.GetOidByTrigger(triggerID)

	if err != nil {
		log.Printf("Error while handling trap [f:%s,t:%s]: OID not found\n", realip.FromRequest(req), triggerID)
		w.WriteHeader(404)
		return
	}

	trigger, err = getTrigger(triggerID) // TODO: lookup in queryCache
	dprintf("<- API [t:%s]", triggerID)
	if err != nil {
		log.Printf("Error while handling trap [f:%s,t:%s]: %s", realip.FromRequest(req), triggerID, err)
		w.WriteHeader(404)
	}

	e := &execution{
		trigger, oid, realip.FromRequest(req), triggerID, triggerID_int,
	}

	if !queuedExecution {
		err := e.handleTrap()
		if err != nil {
			log.Printf("Error while handling trap [f:%s,t:%s]: %s", realip.FromRequest(req), triggerID, err)
		}

		w.WriteHeader(200)
		log.Printf("Trap handled [f:%s,t:%s]\n", realip.FromRequest(req), e.triggerID)

		return
	}

	w.WriteHeader(200)
	log.Printf("Trap handled and added queue [f:%s,t:%s]\n", realip.FromRequest(req), e.triggerID)
	triggerExecution <- e
}

func trapExecutionlistener() {
	timer := time.Duration(conf.SNMP.TrapTimer) * time.Millisecond
	for e := range triggerExecution {
		triggerMux.Lock()
		if lastExecution.IsZero() || (!executeRunning && time.Since(lastExecution) >= timer) {
			err := e.handleTrap()
			if err != nil {
				log.Printf("Error while handling trap [f:%s,t:%s]: %s", e.ip, e.triggerID, err)
			}
			lastExecution = time.Now()
			go executeQueue(time.Duration(conf.SNMP.TrapTimer) * time.Millisecond)
			triggerMux.Unlock()

			continue
		}

		if !executeRunning {
			go executeQueue(timer - time.Since(lastExecution))
		}

		queue = append(queue, e)
		triggerMux.Unlock()
	}
}

func executeQueue(timer time.Duration) {
	executeRunning = true

	<-time.After(timer)
	triggerMux.Lock()

	defer func() {
		executeRunning = false
		triggerMux.Unlock()
	}()

	if len(queue) == 0 {
		return
	}

	var activeExecution *execution
	for _, e := range queue {
		if activeExecution == nil {
			activeExecution = e
			continue
		}

		if e.trigger.Severity < activeExecution.trigger.Severity {
			continue
		}

		if e.trigger.LastChange < activeExecution.trigger.LastChange {
			continue
		}

		activeExecution = e
	}

	queue = nil

	err := activeExecution.handleTrap()
	if err != nil {
		log.Printf("Error while handling trap [f:%s,t:%s]: %s", activeExecution.ip, activeExecution.triggerID, err)
	}

	lastExecution = time.Now()
}

func (e *execution) handleTrap() error {
	// RTFM: http://www.tcpipguide.com/free/t_SNMPVersion1SNMPv1MessageFormat-3.htm
	var payload pdu.Variables

	// SYS_UP_TIME_OID
	payload.Add(value.MustParseOID("1.3.6.1.2.1.1.3.0"), pdu.VariableTypeTimeTicks, time.Since(startTime))
	// SNMP_TRAP_OID
	payload.Add(value.MustParseOID("1.3.6.1.6.3.1.1.4.1.0"), pdu.VariableTypeObjectIdentifier, conf.SNMP.TrapOID)
	// OIDSuffix
	OIDSuffix, err := getOIDSuffix(e.oid)
	if err != nil {
		return err
	}

	intSuffix, err := strconv.Atoi(OIDSuffix)
	if err != nil {
		return fmt.Errorf("Error while handling trap [f:%s,t:%s]: invalid OIDSuffix %s", e.ip, e.triggerID, OIDSuffix)
	}

	payload.Add(value.MustParseOID(fmt.Sprintf(e.oid, TP_OIDSUFFIX)), pdu.VariableTypeInteger, int32(intSuffix))
	// TriggerID
	payload.Add(value.MustParseOID(fmt.Sprintf(e.oid, TP_ID)), pdu.VariableTypeInteger, int32(e.triggerID_int))
	// Expression
	payload.Add(value.MustParseOID(fmt.Sprintf(e.oid, TP_EXPRESSION)), pdu.VariableTypeOctetString, e.trigger.Expression)
	// Description
	payload.Add(value.MustParseOID(fmt.Sprintf(e.oid, TP_DESCRIPTION)), pdu.VariableTypeOctetString, e.trigger.Description)
	// Severity
	payload.Add(value.MustParseOID(fmt.Sprintf(e.oid, TP_SEVERITY)), pdu.VariableTypeInteger, int32(e.trigger.Severity))
	// State
	payload.Add(value.MustParseOID(fmt.Sprintf(e.oid, TP_STATE)), pdu.VariableTypeInteger, int32(e.trigger.State))
	// Enabled
	enabled := 0
	if e.trigger.Enabled {
		enabled = 1
	}

	payload.Add(value.MustParseOID(fmt.Sprintf(e.oid, TP_ENABLED)), pdu.VariableTypeInteger, int32(enabled))
	// AlarmState
	payload.Add(value.MustParseOID(fmt.Sprintf(e.oid, TP_ALARMSTATE)), pdu.VariableTypeInteger, int32(e.trigger.AlarmState))
	// LastChange
	payload.Add(value.MustParseOID(fmt.Sprintf(e.oid, TP_LASTCHANGE)), pdu.VariableTypeInteger, int32(e.trigger.LastChange))
	// HostNames
	payload.Add(value.MustParseOID(fmt.Sprintf(e.oid, TP_HOSTNAME)), pdu.VariableTypeOctetString, getHostnamesString(e.trigger))

	if err := randSession(sessions).Notify(payload); err != nil {
		log.Fatalf(errgo.Details(err))
	}

	return nil
}

func init() {
	triggerExecution = make(chan *execution)
}
