APP=zabbix-snmp-gateway

VERSION=1.2.3
RELEASE=0
BUILD_TIME=`date +%s`

LDFLAGS=-ldflags "-w -s -X main.VERSION=${VERSION} -X main.BUILD_TIME=${BUILD_TIME}"

all: clean linux build zip

linux:
	env GOOS=linux GOARCH=amd64 go build -v ${LDFLAGS} -o ${APP}.linux.amd64

build:
	go build -v ${LDFLAGS} -o ${APP}

clean:
	rm -f ${APP}
	rm -f ${APP}.linux.amd64
	rm -f ${APP}-*.zip
	rm -rf rpm/
	find . -name '.DS_Store' -type f -delete

zip:
	find . -name '.DS_Store' -type f -delete
	zip ${APP}-${VERSION}.zip -9 -r ./etc/ ./README.md LICENSE.md zabbix-snmp-gateway.linux.amd64 snmp-gateway.json

rpm:
	rm -rf ./rpm
	mkdir -p rpm/{BUILD,RPMS,SOURCES,SPECS,SRPMS}

	find . -name '.DS_Store' -type f -delete
	tar -cz --transform='s,^,/${APP}/,' -vf rpm/SOURCES/${APP}.tar.gz \
            etc/logrotate.d/zabbix-snmp-gateway \
            etc/rsyslog.d/zabbix-snmp-gateway.conf \
            etc/systemd/system/zabbix-snmp-gateway.service \
            snmp-gateway.json \
            ${APP}.linux.amd64

	rpmbuild -v -bb zabbix-snmp-gateway.spec \
	--define "_topdir `pwd`/rpm" \
	--define "buildtime ${BUILD_TIME}" \
	--define "version ${VERSION}" \
	--define "release ${RELEASE}" \
	--define "arch amd64" \
	--target="x86_64" && echo "Done"

.PHONY: linux build clean zip rpm
