/*
** Zabbix
** Copyright (C) 2001-2018 Zabbix SIA
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, version 3.
**
** This program is distributed in the hope that it will be useful, but
** WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"sort"
	"strconv"
	"sync"
	"time"

	"strings"

	"github.com/vadimipatov/go-agentx/value"
	"github.com/vadimipatov/go-zabbix"
)

type Host struct {
	HostID string `json:"HostID"`
	Name   string `json:"Name"`
}

type Meta struct {
	TriggerID   string `json:"TriggerID"`
	Hosts       []Host `json:"Hosts"`
	Description string `json:"Description"`
}

type MappingTable struct {
	BuildTime time.Time                  `json:"BuildTime"`
	Oids      map[string]map[string]Meta `json:"Data"`
	Triggers  map[string]string          `json:"-"`
	Elements  int                        `json:"Elements"`
	sync.RWMutex
}

func CreateHostMeta(zHosts []zabbix.Host) []Host {
	var hosts []Host
	for _, h := range zHosts {
		hosts = append(hosts, Host{
			HostID: h.HostID,
			Name:   h.Hostname,
		})
	}
	return hosts
}

func (this *MappingTable) Rebuild(data []zabbix.Trigger, groups map[string]string, baseOID string) {
	this.RLock()
	defer this.RUnlock()

	var (
		OIDGroups  []string
		OIDSuffix  string
		triggerOID string
		oids       map[string]map[string]Meta
		triggers   map[string]string
		elements   int
	)

	this.BuildTime = time.Now()

	triggers = make(map[string]string, 0)
	oids = make(map[string]map[string]Meta, 0)
	oids[defaultGroup] = make(map[string]Meta)
	elements = 0

	for _, trigger := range data {
		OIDGroups = OIDGroups[:0]
		OIDSuffix = ""

		for _, tag := range trigger.Tags {
			if tag.Name == "OIDSuffix" {
				OIDSuffix = tag.Value
			}
			if tag.Name == "OIDGroup" {
				OIDGroups = append(OIDGroups, tag.Value)
			}
		}

		if len(OIDSuffix) == 0 {
			continue
		}

		if _, err := strconv.Atoi(OIDSuffix); err != nil {
			log.Printf("Warning: trigger %s have invalid OIDSuffix: %s\n", trigger.TriggerID, OIDSuffix)
			continue
		}

		if len(OIDGroups) > 0 {
			if len(OIDGroups) > 1 {
				sort.Strings(OIDGroups)
				log.Printf("Warning: multiple OIDGroups are defined for trigger %s\n", trigger.TriggerID)
			}
			if groupOID, ok := groups[OIDGroups[0]]; ok {
				triggerOID = groupOID + ".%d." + OIDSuffix
				printableOID := groupOID + ".X." + OIDSuffix
				if _, err := value.ParseOID(fmt.Sprintf(triggerOID, TP_OIDSUFFIX)); err == nil {
					if _, exist := oids[OIDGroups[0]]; !exist {
						oids[OIDGroups[0]] = make(map[string]Meta)
					}
					if _, exist := oids[OIDGroups[0]][triggerOID]; !exist {
						oids[OIDGroups[0]][triggerOID] = Meta{
							TriggerID:   trigger.TriggerID,
							Hosts:       CreateHostMeta(trigger.Hosts),
							Description: trigger.Description,
						}
						triggers[trigger.TriggerID] = triggerOID
						elements++
					} else {
						log.Printf("Warning: triggers %s and %s have identical OID: %s \n",
							oids[OIDGroups[0]][triggerOID].TriggerID, trigger.TriggerID, printableOID)
					}
				} else {
					log.Printf("Warning: trigger %s have invalid OID: %s\n", trigger.TriggerID, printableOID)
				}
			} else {
				goto useBaseOID
			}
			continue
		}

	useBaseOID:
		triggerOID = baseOID + ".%d." + OIDSuffix
		printableOID := baseOID + ".X." + OIDSuffix
		if _, err := value.ParseOID(fmt.Sprintf(triggerOID, TP_OIDSUFFIX)); err == nil {
			if _, exist := oids[defaultGroup][triggerOID]; !exist {
				oids[defaultGroup][triggerOID] = Meta{
					TriggerID:   trigger.TriggerID,
					Hosts:       CreateHostMeta(trigger.Hosts),
					Description: trigger.Description,
				}
				triggers[trigger.TriggerID] = triggerOID
				elements++
			} else {
				log.Printf("Warning: triggers %s and %s have identical OID: %s \n",
					oids[defaultGroup][triggerOID].TriggerID, trigger.TriggerID, printableOID)
			}
		} else {
			log.Printf("Warning: trigger %s have invalid OID: %s\n", trigger.TriggerID, printableOID)
		}
	}

	this.Elements = elements
	this.Oids = oids
	this.Triggers = triggers
}

func (this *MappingTable) RebuildProblemTriggers(data []zabbix.Trigger, snmp Snmp) {
	this.Lock()
	defer this.Unlock()

	this.Oids[problemGroup] = make(map[string]Meta)

	for _, trigger := range data {
		if snmp.ProblemBaseOID != "" && !isProblemTrigger(trigger, snmp.ProblemTagFilter) {
			continue
		}

		OIDSuffix := trigger.TriggerID
		if len(OIDSuffix) == 0 {
			continue
		}

		if _, err := strconv.Atoi(OIDSuffix); err != nil {
			log.Printf("Warning: trigger %s have invalid OIDSuffix: %s\n", trigger.TriggerID, OIDSuffix)
			continue
		}

		triggerOID := snmp.ProblemBaseOID + ".%d." + OIDSuffix
		printableOID := snmp.ProblemBaseOID + ".X." + OIDSuffix

		if _, err := value.ParseOID(fmt.Sprintf(triggerOID, TP_OIDSUFFIX)); err != nil {
			log.Printf("Warning: trigger %s have invalid OID: %s\n", trigger.TriggerID, printableOID)
			continue
		}

		if _, exist := this.Oids[problemGroup][triggerOID]; exist {
			log.Printf("Warning: triggers %s and %s have identical OID: %s \n",
				this.Oids[problemGroup][triggerOID].TriggerID, trigger.TriggerID, printableOID)
			continue
		}

		this.Oids[problemGroup][triggerOID] = Meta{
			TriggerID:   trigger.TriggerID,
			Hosts:       CreateHostMeta(trigger.Hosts),
			Description: trigger.Description,
		}

		this.Elements++
	}
}

func isProblemTrigger(trigger zabbix.Trigger, filter string) bool {
	if trigger.AlarmState == zabbix.TriggerAlarmStateOK {
		return false
	}

	if trigger.Severity < conf.SNMP.ProblemMinSeverity {
		return false
	}

	if filter == "" {
		return true
	}

	for _, tag := range trigger.Tags {
		if tag.Name == filter {
			return true
		}
	}

	return false
}

func (this *MappingTable) GetOids(group string) map[string]string {
	var oids map[string]string
	oids = make(map[string]string)
	for k, v := range this.Oids[group] {
		oids[k] = v.TriggerID
	}
	return oids
}

func (this *MappingTable) GetOidByTrigger(triggerID string) (string, error) {
	if oid, ok := this.Triggers[triggerID]; ok {
		return oid, nil
	}
	return "", errors.New("oid not found")
}

func (this *MappingTable) IsExpired(expiry time.Duration) bool {
	return time.Since(this.BuildTime) >= expiry
}

func (this *MappingTable) GetBuildTime() int64 {
	return this.BuildTime.Unix()
}

func (this *MappingTable) GetElementsCount() int {
	return this.Elements
}

func (this *MappingTable) SaveToDisk() {
	// TODO
}

func (this *MappingTable) LoadFromDisk() {
	// TODO
}

func (this *MappingTable) MarshalJSON() []byte {
	j, err := json.Marshal(this)
	if err != nil {
		log.Println(err)
		return nil
	}

	return []byte(strings.Replace(strings.Replace(string(j), "%d", "X", -1), "::", "", -1))
}
