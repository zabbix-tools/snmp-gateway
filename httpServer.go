/*
** Zabbix
** Copyright (C) 2001-2018 Zabbix SIA
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, version 3.
**
** This program is distributed in the hope that it will be useful, but
** WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

package main

import (
	"log"
	"net"
	"net/http"

	"gopkg.in/errgo.v1"
)

func setupHandlers() {
	if conf.SNMP.TrapTimer != 0 {
		queuedExecution = true
		go trapExecutionlistener()
	}

	serveMux.HandleFunc("/trap", trapHandler)

	if conf.InternalStatus.Enabled {
		serveMux.Handle(conf.InternalStatus.Endpoint, setupMonitoring())
	}

	serveMux.HandleFunc("/debug/maptable", func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		w.Write(mapTable.MarshalJSON())
	})
}

func startHTTP() {
	serveMux = http.NewServeMux()
	setupHandlers()
	httpListener, err := net.Listen(conf.HTTP.Net, conf.HTTP.Listen)
	if err != nil {
		log.Fatalln(errgo.Details(err))
	}
	log.Println("HTTP server started on", conf.HTTP.Listen)
	readyChan <- struct{}{}
	log.Fatalln(http.Serve(httpListener, serveMux))
}
