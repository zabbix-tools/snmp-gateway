/*
** Zabbix
** Copyright (C) 2001-2018 Zabbix SIA
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, version 3.
**
** This program is distributed in the hope that it will be useful, but
** WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

package main

import (
	"errors"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"strconv"

	"github.com/vadimipatov/go-agentx"
	"github.com/vadimipatov/go-agentx/value"
	"github.com/vadimipatov/go-zabbix"
	"gopkg.in/errgo.v1"
)

const defaultGroup = "::default::"
const problemGroup = "::problem::"
const hostGroups = "::hostgroups::"

type hostgroup struct {
	Name      string
	ID        string
	OIDSuffix string
}

var (
	conf             = &Configuration{}
	mapTable         MappingTable
	zbxapi           *zabbix.Session
	serveMux         *http.ServeMux
	transactionCache TCache
	queryCache       QCache
	sessions         map[string]*agentx.Session
	VERSION          = "undefined"
	BUILD_TIME       = "undefined"
	startTime        = time.Now().UTC()
	readyChan        chan struct{}
	hostgroups       map[string]hostgroup
)

func rebuildCache() error {
	var hgList []string

	// hostgroups
	for hgName := range conf.SNMP.Hostgroups.Groups {
		hgList = append(hgList, hgName)
	}

	allGroups, err := zbxapi.GetHostgroups(zabbix.HostgroupGetParams{
		GetParameters: zabbix.GetParameters{
			Filter: map[string]interface{}{
				"name": hgList,
			},
			OutputFields: zabbix.SelectFields{"groupid", "name"},
		},
	})

	if (err != nil && strings.Contains(fmt.Sprint(err), "No results were found")) || len(allGroups) == 0 {
		log.Println("Hostgroups not found")
	} else {
		if err != nil {
			log.Println("Can't get a list of hostgroups:", err)
		} else {
			if len(allGroups) != len(conf.SNMP.Hostgroups.Groups) {
				log.Println("One or more configured hostgroups were not found")
			}
		}
	}

	hostgroups = make(map[string]hostgroup, 0)
	for _, g := range allGroups {
		hostgroups[g.Name] = hostgroup{
			g.Name,
			g.GroupID,
			conf.SNMP.Hostgroups.Groups[g.Name],
		}
	}

	// triggers
	allTriggers, err := zbxapi.GetTriggers(zabbix.TriggerGetParams{
		SelectTags:        "extend",
		SelectHosts:       []string{"hostid", "host"},
		ExpandDescription: true,
		ExpandExpression:  true,
	})

	if err == nil && len(allTriggers) == 0 {
		err = errors.New("triggers not found")
	}

	if err != nil {
		log.Println("Mapping table build failed:", err)
		return err
	}

	mapTable.Rebuild(allTriggers, conf.SNMP.OIDGroups, conf.SNMP.BaseOID)

	err = handleProblemTriggers()
	if err != nil {
		log.Println("Mapping table build failed:", err)
		return err
	}

	if mapTable.GetElementsCount() == 0 {
		err = errors.New("no data for caching")
		log.Println("Mapping table build failed:", err)
		return err
	}

	log.Printf("Mapping table builded (%d elements cached)", mapTable.GetElementsCount())

	return nil
}

func handleProblemTriggers() error {
	if conf.SNMP.ProblemBaseOID == "" {
		return nil
	}

	problemTriggers, err := zbxapi.GetTriggers(zabbix.TriggerGetParams{
		SelectTags:                      "extend",
		SelectHosts:                     []string{"hostid", "host"},
		ExpandDescription:               true,
		ExpandExpression:                true,
		WithLastEventUnacknowledgedOnly: conf.SNMP.ProblemHideAck,
	})

	if err != nil {
		log.Println("Mapping table build failed:", err)
		return err
	}

	if len(problemTriggers) > 0 {
		mapTable.RebuildProblemTriggers(problemTriggers, conf.SNMP)
	}

	return nil
}

func rebuildCacheAndSchedule() {
	rebuildCache()
	assignHandlers(mapTable)
	if conf.MappingTable.RebuildInterval > 0 {
		go func() {
			for range time.Tick(conf.MappingTable.RebuildInterval) {
				rebuildCache()
				assignHandlers(mapTable)
			}
		}()
	}
}

func getOIDGroupList() []string {
	list := make([]string, 0, len(conf.SNMP.OIDGroups)+1)
	for k := range conf.SNMP.OIDGroups {
		list = append(list, k)
	}

	list = append(list, defaultGroup)
	if conf.SNMP.ProblemBaseOID != "" {
		list = append(list, problemGroup)
	}

	return list
}

func newAgentXSession(name string, oid value.OID) (session *agentx.Session) {
	var (
		agent *agentx.Client
		err   error
	)

	agent = &agentx.Client{
		Net:               conf.AgentX.Net,
		Address:           conf.AgentX.Address,
		Timeout:           conf.AgentX.Timeout,
		ReconnectInterval: conf.AgentX.ReconnectInterval,
	}

	agent.Name = name
	agent.NameOID = oid

	if err = agent.Open(); err != nil {
		log.Fatalln(errgo.Details(err))
	}

	session, err = agent.Session()
	if err != nil {
		log.Fatalln(errgo.Details(err))
	}

	if err := session.Register(127, oid); err != nil {
		log.Fatalln(errgo.Details(err))
	}

	log.Printf("Subtree %s binded to %s", oid, strings.Replace(name, "::", "", -1))

	return
}

func startAgentX() {
	var oid string

	sessions = make(map[string]*agentx.Session)

	for _, group := range getOIDGroupList() {
		switch group {
		case defaultGroup:
			oid = conf.SNMP.BaseOID
		case problemGroup:
			oid = conf.SNMP.ProblemBaseOID
		default:
			oid = conf.SNMP.OIDGroups[group]
		}

		sessions[group] = newAgentXSession(group, value.MustParseOID(oid))
	}

	sessions[hostGroups] = newAgentXSession(hostGroups, value.MustParseOID(conf.SNMP.Hostgroups.BaseOID))

	log.Println("Connected to snmpd")
}

func getOIDSuffix(oid string) (string, error) {
	splitted := strings.Split(oid, "%d.")
	if len(splitted) == 2 {
		return splitted[1], nil
	} else {
		return "", errgo.Newf("wrong OID: ", oid)
	}
}

// OIDs handlers fabric
func createOIDSuffixHandler(oid string) func(transactionID string) interface{} {
	return func(transactionID string) interface{} {
		suffix, err := getOIDSuffix(oid)
		if err != nil {
			return err
		}
		intSuffix, err := strconv.Atoi(suffix)
		if err != nil {
			return errors.New("invalid OIDSuffix: " + suffix)
		}
		return int32(intSuffix)
	}
}

func assignHandlers(mapTable MappingTable) {
	for _, group := range getOIDGroupList() {
		axHandler := &AgentXHandler{}
		for oid, triggerID := range mapTable.GetOids(group) {
			if group == problemGroup {
				for i, param := range ProblemTriggerParamsList {
					axHandler.Register(fmt.Sprintf(oid, i), createTriggerHandler(triggerID, param))
				}

				continue
			}

			axHandler.Register(fmt.Sprintf(oid, 1), createOIDSuffixHandler(oid))
			for i, param := range TriggerParamsList {
				axHandler.Register(fmt.Sprintf(oid, i+2), createTriggerHandler(triggerID, param))
			}
		}
		sessions[group].Handler = axHandler
	}

	axHandler := &AgentXHandler{}
	for _, hg := range hostgroups {
		oid := fmt.Sprintf("%s.%%d.%s", conf.SNMP.Hostgroups.BaseOID, hg.OIDSuffix)
		axHandler.Register(fmt.Sprintf(oid, 1), createOIDSuffixHandler(oid))
		for i, param := range HostgroupParamsList {
			axHandler.Register(fmt.Sprintf(oid, i+2), createHGHandler(hg.Name, param))
		}
	}
	sessions[hostGroups].Handler = axHandler
}

func init() {
	var (
		configPath string
		err        error
	)

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "%s version %s\n", os.Args[0], VERSION)
		fmt.Fprintf(os.Stderr, "Built %s\n", BUILD_TIME)
		fmt.Fprintln(os.Stderr, "Usage:")
		flag.PrintDefaults()
	}

	// Get configuration
	flag.StringVar(&configPath, "c", "snmp-gateway.json", "Config file")
	flag.Parse()

	conf, err = loadConf(configPath)
	if err != nil {
		log.Fatalln(err)
	}

	if conf.MappingTable.AllowManualRebuild {
		s := make(chan os.Signal, 1)
		signal.Notify(s, syscall.SIGUSR2)
		go func() {
			for {
				<-s
				rebuildCache()
				assignHandlers(mapTable)
			}
		}()
	}

	transactionCache = NewTCache(len(conf.SNMP.OIDGroups) + 2)
	queryCache = NewQCache(conf.CacheLifeTime)

	readyChan = make(chan struct{})
}

func main() {
	var err error

	// Setup Zabbix API
	zbxapi, err = zabbix.NewSession(conf.ZabbixAPI.Endpoint, conf.ZabbixAPI.User, conf.ZabbixAPI.Secret, conf.AgentX.Timeout)
	if err != nil {
		log.Fatalln(err)
	}
	log.Printf("Connected to Zabbix API v%s\n", zbxapi.Version())

	// Setup AgentX
	startAgentX()

	// Build cache for first time, assign agentx handlers and schedule doing this task periodically
	rebuildCacheAndSchedule()

	// Setup HTTP server
	go startHTTP()
	<-readyChan

	log.Println("Ready")

	select {} // main loop
}
