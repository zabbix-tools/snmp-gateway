/*
** Zabbix
** Copyright (C) 2001-2018 Zabbix SIA
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, version 3.
**
** This program is distributed in the hope that it will be useful, but
** WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

package main

import (
	"strconv"

	"github.com/vadimipatov/go-zabbix"
	"gopkg.in/errgo.v1"
	"strings"
	"fmt"
)

type HGStat struct {
	GroupID string
	Name    string
	Alarms  [6]int
}

var HostgroupParamsList = []string{
	"groupid",
	"name",
	"disaster",
	"high",
	"average",
	"warning",
	"information",
	"notClassified",
}

// getHGStat
func getHGStat(groupName string) (*HGStat, error) {
	var (
		stat HGStat
		getParams zabbix.GetParameters
	)

	response := make([]zabbix.Trigger, 0)

	if !conf.SNMP.Hostgroups.CountUnknown {
		getParams = zabbix.GetParameters{
			Filter: map[string]interface{}{
				"state": zabbix.TriggerStateNormal,
			},
		}
	}

	response, err := zbxapi.GetTriggers(zabbix.TriggerGetParams{
		ExpandDescription: false,
		ExpandExpression:  false,
		Group:             groupName,
		SelectGroups:      []string{"groupid", "name"},
		ActiveOnly: 	   true,
		RecentProblemOnly: true,
		SkipDependent:     true, // is it right?
		GetParameters: 	   getParams,
	})

	if err != nil && !strings.Contains(fmt.Sprint(err), "No results were found") {
		return nil, err
	}

	stat.Name = groupName
	stat.GroupID = hostgroups[groupName].ID

	for _, trigger := range response {
		if _, err := mapTable.GetOidByTrigger(trigger.TriggerID); err == nil {
			t := trigger
			queryCache.Push(t.TriggerID, &t)
		}

		for _, group := range trigger.Groups {
			if group.GroupID == stat.GroupID {
				if trigger.AlarmState == zabbix.TriggerAlarmStateProblem {
					stat.Alarms[trigger.Severity]++
				}
				break
			}
		}
	}

	return &stat, nil
}

// Hostgroup handlers fabric
func createHGHandler(groupName string, param string) func(transactionID string) interface{} {
	return func(transactionID string) interface{} {
		var (
			stat *HGStat
			err  error
		)

		result := transactionCache.Get(transactionID, groupName)
		if result != nil {
			dprintf("<- transactionCache [g:%s,p:%s]", groupName, param)
			stat = result.(*HGStat)
		} else {
			result = queryCache.Get(groupName)
			if result != nil {
				dprintf("<- queryCache [g:%s,p:%s]", groupName, param)
				stat = result.(*HGStat)
				transactionCache.Push(transactionID, groupName, stat)
			} else {
				stat, err = getHGStat(groupName)
				dprintf("<- API [g:%s,p:%s]", groupName, param)
				if err != nil {
					return errgo.Newf("error while getting triggers data for group '%s': %s", groupName, err)
				}
				transactionCache.Push(transactionID, groupName, stat)
				queryCache.Push(groupName, stat)
			}
		}

		if stat == nil {
			return errgo.Newf("error while getting triggers data for group '%s'", groupName)
		}

		switch param {
		case "groupid":
			v, err := strconv.Atoi(stat.GroupID)
			if err != nil {
				return err
			}
			return int32(v)
		case "name":
			return stat.Name
		case "disaster":
			return int32(stat.Alarms[5])
		case "high":
			return int32(stat.Alarms[4])
		case "average":
			return int32(stat.Alarms[3])
		case "warning":
			return int32(stat.Alarms[2])
		case "information":
			return int32(stat.Alarms[1])
		case "notClassified":
			return int32(stat.Alarms[0])
		default:
			return errgo.Newf("unknown param '%s'", param)
		}
	}
}
