/*
** Zabbix
** Copyright (C) 2001-2018 Zabbix SIA
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, version 3.
**
** This program is distributed in the hope that it will be useful, but
** WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

package main

import (
	"sync"

	"github.com/Focinfi/gcircularqueue"
)

type TCache struct {
	length int
	cq     *gcircularqueue.CircularQueueThreadSafe
	hits   uint64
	misses uint64
	data   map[string]map[string]interface{}
	sync.RWMutex
}

func NewTCache(length int) TCache {
	tc := TCache{}
	tc.length = length
	tc.cq = gcircularqueue.NewCircularQueueThreadSafe(length)
	tc.data = make(map[string]map[string]interface{})
	return tc
}

func (this *TCache) Push(transactionID string, entityID string, entity interface{}) {
	this.Lock()
	defer this.Unlock()
	if _, ok := this.data[transactionID]; !ok {
		this.data[transactionID] = make(map[string]interface{})
		this.cq.Push(transactionID)
	}
	this.data[transactionID][entityID] = entity

	if this.cq.IsFull() {
		delete(this.data, this.cq.Shift().(string))
	}
}

func (this *TCache) Get(transactionID string, entityID string) interface{} {
	this.Lock()
	defer this.Unlock()
	if entity, ok := this.data[transactionID][entityID]; ok {
		this.hits++
		return entity
	}
	this.misses++
	return nil
}

func (this *TCache) GetCacheStat() (uint64, uint64) {
	// TODO, fix: counters -> persec
	this.RLock()
	defer this.RUnlock()
	return this.hits, this.misses
}

func (this *TCache) Length() int {
	this.RLock()
	defer this.RUnlock()
	return len(this.data)
}
