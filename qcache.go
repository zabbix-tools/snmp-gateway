/*
** Zabbix
** Copyright (C) 2001-2018 Zabbix SIA
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, version 3.
**
** This program is distributed in the hope that it will be useful, but
** WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

package main

import (
	"sync"
	"time"
)

type CacheElem struct {
	created time.Time
	content interface{}
	expired bool
	sync.RWMutex
}

func newCacheElem(content interface{}) CacheElem {
	e := CacheElem{}
	e.Set(content)
	return e
}

func (this *CacheElem) Set(content interface{}) {
	this.Lock()
	this.content = content
	this.created = time.Now()
	this.expired = false
	this.Unlock()
}

func (this *CacheElem) MarkExpired() {
	this.Lock()
	this.expired = true
	this.Unlock()
}

func (this *CacheElem) Get() (interface{}, time.Time) {
	this.RLock()
	defer this.RUnlock()
	var content interface{} = nil
	if !this.expired {
		content = this.content
	}
	return content, this.created
}

// ***

type QCache struct {
	lifeTime time.Duration
	hits     uint64
	misses   uint64
	data     map[string]*CacheElem
	sync.RWMutex
}

func NewQCache(lifeTime time.Duration) QCache {
	qc := QCache{}
	qc.lifeTime = lifeTime
	qc.data = make(map[string]*CacheElem)
	return qc
}

func (this *QCache) Push(id string, content interface{}) {
	this.Lock()
	_, ok := this.data[id]
	if !ok {
		el := newCacheElem(content)
		this.data[id] = &el
	} else {
		this.data[id].Set(content)
	}
	this.Unlock()
}

func (this *QCache) Get(id string) interface{} {
	this.Lock()
	defer this.Unlock()
	if el, ok := this.data[id]; ok {
		content, created := el.Get()
		if time.Since(created) >= this.lifeTime || content == nil {
			el.MarkExpired()
			this.misses++
			return nil
		}
		this.hits++
		return content
	}
	this.misses++
	return nil
}

func (this *QCache) GetCacheStat() (uint64, uint64, int) {
	// TODO, fix: counters -> persec
	this.RLock()
	defer this.RUnlock()
	return this.hits, this.misses, this.Length()
}

func (this *QCache) Length() int {
	this.RLock()
	defer this.RUnlock()
	return len(this.data)
}
