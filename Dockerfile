FROM golang:alpine as build_env
LABEL maintainer="Alexey Pustovalov <alexey.pustovalov@zabbix.com>"

ARG BUILD_DATE
ARG VCS_REF

ARG APK_FLAGS_COMMON="-q"
ARG APK_FLAGS_PERSISTANT="${APK_FLAGS_COMMON} --clean-protected --no-cache"
ARG APK_FLAGS_DEV="${APK_FLAGS_COMMON} --no-cache"
ENV TERM=xterm

STOPSIGNAL SIGTERM

WORKDIR $GOPATH/src/zabbix/zabbix-snmp-gateway

RUN apk update && \
    apk add ${APK_FLAGS_DEV} --virtual build-dependencies \
            git \
            make \
            subversion && \
    mkdir -p $GOPATH/src/zabbix/ && \
    cd "$GOPATH/src/zabbix/" && \
    ls -lah && \
    svn --quiet --force export svn://192.168.3.4/zabbix-snmp-gateway/trunk zabbix-snmp-gateway 1>/dev/null && \
    cd $GOPATH/src/zabbix/zabbix-snmp-gateway && \
    go get && \
    make && \
    cp $GOPATH/src/zabbix/zabbix-snmp-gateway/zabbix-snmp-gateway /tmp/ && \
    cp $GOPATH/src/zabbix/zabbix-snmp-gateway/snmp-gateway.json /tmp/ && \
    apk del ${APK_FLAGS_COMMON} --purge \
            build-dependencies && \
    rm -rf /var/cache/apk/* && \
    rm -rf /root/.subversion && \
    rm -rf /var/svn

FROM alpine:latest..

WORKDIR /usr/sbin

RUN mkdir -p /etc/zabbix/

COPY --from=build_env /tmp/zabbix-snmp-gateway .
COPY --from=build_env /tmp/snmp-gateway.json /etc/zabbix/

CMD ["zabbix-snmp-gateway"]