/*
** Zabbix
** Copyright (C) 2001-2018 Zabbix SIA
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, version 3.
**
** This program is distributed in the hope that it will be useful, but
** WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

package main

// Configuration best practices:
// https://medium.com/@felipedutratine/best-practices-for-configuration-file-in-your-code-2d6add3f4b86

import (
	"path"
	"strconv"
	"strings"
	"time"

	"github.com/spf13/viper"
	"github.com/vadimipatov/go-agentx/value"
	"gopkg.in/errgo.v1"
)

type Configuration struct {
	AgentX struct {
		Address           string
		Net               string
		ReconnectInterval time.Duration
		Timeout           time.Duration
	}
	HTTP struct {
		Net    string
		Listen string
	}
	ConfigFile    string
	CacheLifeTime time.Duration
	MappingTable  struct {
		RebuildInterval    time.Duration
		AllowManualRebuild bool
	}
	InternalStatus struct {
		Enabled  bool
		ShowConf bool
		Endpoint string
	}
	SNMP      Snmp
	ZabbixAPI struct {
		Secret   string `json:"-"`
		Timeout  time.Duration
		Endpoint string
		User     string `json:"-"`
	}
}

type Snmp struct {
	BaseOID            string
	TrapOID            string
	ProblemBaseOID     string
	ProblemMinSeverity int
	ProblemHideAck     bool
	ProblemTagFilter   string
	TrapTimer          int
	OIDGroups          map[string]string
	Hostgroups         struct {
		CountUnknown bool
		BaseOID      string
		Groups       map[string]string
	}
}

type ConfigPath struct {
	Path     string
	FileName string
	Ext      string
}

// validateConf Validate configuration
func validateConf(conf *Configuration) error {
	if conf.SNMP.ProblemBaseOID != "" {
		if _, err := value.ParseOID(conf.SNMP.ProblemBaseOID); err != nil {
			return errgo.Newf("Invalid ProblemOID: %s", conf.SNMP.ProblemBaseOID)
		}
	}

	if conf.SNMP.ProblemTagFilter != "" && len(conf.SNMP.ProblemTagFilter) > 255 {
		return errgo.Newf("Invalid ProblemTagFilter: %s", conf.SNMP.ProblemTagFilter)
	}

	if conf.SNMP.TrapTimer < 0 {
		return errgo.Newf("Invalid TrapTimer: %s", conf.SNMP.TrapTimer)
	}

	if _, err := value.ParseOID(conf.SNMP.BaseOID); err != nil {
		return errgo.Newf("Invalid BaseOID: %s", conf.SNMP.BaseOID)
	}

	for g, o := range conf.SNMP.OIDGroups {
		if _, err := value.ParseOID(o); err != nil {
			return errgo.Newf("Invalid OID for group %s: %s", g, o)
		}
	}

	for g, o := range conf.SNMP.Hostgroups.Groups {
		if _, err := strconv.Atoi(o); err != nil {
			return errgo.Newf("Invalid OIDSuffix for group %s: %s", g, o)
		}
	}

	if conf.SNMP.BaseOID != "" &&
		conf.SNMP.ProblemBaseOID != "" &&
		conf.SNMP.BaseOID == conf.SNMP.ProblemBaseOID {
		return errgo.Newf("Invalid BaseOID and ProblemBaseOID must be unique: %s", conf.SNMP.BaseOID)
	}

	// TODO: check for the uniqueness of Names, BaseOIDs, OIDSuffixes
	// return errgo.Newf("BaseOIDs should be unique")

	// TODO: check addresses
	//       ports
	//       urls
	//       timeouts (>0)
	//       intervals (>0)

	return nil
}

// setDefaults Set default values for not declared params
func setDefaults(v *(viper.Viper)) {
	defaults := map[string]interface{}{
		"MappingTable": map[string]interface{}{
			"RebuildInterval":    3600,
			"AllowManualRebuild": true,
		},
		"CacheLifeTime": 5,
		"HTTP": map[string]interface{}{
			"Net":    "tcp4",
			"Listen": "127.0.0.1:3000",
		},
		"InternalStatus": map[string]interface{}{
			"Enabled":  true,
			"ShowConf": true,
			"Endpoint": "/debug/vars",
		},
		"AgentX": map[string]interface{}{
			"Net":               "unix",
			"Address":           "/var/agentx/master",
			"Timeout":           15,
			"ReconnectInterval": 1,
		},
		"ZabbixAPI": map[string]interface{}{
			"Endpoint": "http://127.0.0.1/zabbix/api_jsonrpc.php",
			"User":     "Admin",
			"Secret":   "zabbix",
			"Timeout":  10,
		},
		"SNMP": map[string]interface{}{
			"BaseOID":            "1.3.6.1.4.1.3043.7.55",
			"TrapOID":            "1.3.6.1.4.1.3043.0",
			"ProblemBaseOID":     "",
			"ProblemMinSeverity": 0,
			"ProblemHideAck":     false,
			"ProblemTagFilter":   "",
			"TrapTimer":          0,
			"OIDGroups":          []interface{}{},
			"Hostgroups": map[string]interface{}{
				"CountUnknown": false,
				"BaseOID":      "1.3.6.1.4.1.3046.7.55",
				"Groups":       []interface{}{},
			},
		},
	}
	for key, value := range defaults {
		v.SetDefault(key, value)
	}
}

// loadConf Load configuration from file and env vars
func loadConf(configPath string) (*Configuration, error) {
	v := viper.New()
	conf := Configuration{}

	// Setup config file
	configFileComponents := ConfigPath{}
	configFileComponents.Path, configFileComponents.FileName = path.Split(configPath)
	configFileComponents.Ext = path.Ext(configFileComponents.FileName)
	configFileComponents.FileName =
		configFileComponents.FileName[:len(configFileComponents.FileName)-len(configFileComponents.Ext)]

	v.SetConfigName(configFileComponents.FileName)

	if len(configFileComponents.Path) == 0 {
		v.AddConfigPath(".")
		v.AddConfigPath("/etc/zabbix/")
	} else {
		v.AddConfigPath(configFileComponents.Path)
	}

	if len(configFileComponents.Ext) > 0 {
		v.SetConfigType(configFileComponents.Ext[1:])
	}

	err := v.ReadInConfig()
	if err != nil {
		return nil, err
	}
	conf.ConfigFile = v.ConfigFileUsed()

	replacer := strings.NewReplacer(".", "_")
	v.SetEnvKeyReplacer(replacer)

	setDefaults(v)

	v.SetEnvPrefix("SNMPGW")
	v.AutomaticEnv()

	//v.Debug()

	conf.MappingTable.RebuildInterval = v.GetDuration("MappingTable.RebuildInterval") * time.Second
	conf.MappingTable.AllowManualRebuild = v.GetBool("MappingTable.AllowManualRebuild")

	conf.CacheLifeTime = v.GetDuration("CacheLifeTime") * time.Second

	conf.InternalStatus.Enabled = v.GetBool("InternalStatus.Enabled")
	conf.InternalStatus.ShowConf = v.GetBool("InternalStatus.ShowConf")
	conf.InternalStatus.Endpoint = strings.TrimSpace(v.GetString("InternalStatus.Endpoint"))

	conf.HTTP.Net = strings.TrimSpace(v.GetString("HTTP.Net"))
	conf.HTTP.Listen = strings.TrimSpace(v.GetString("HTTP.Listen"))

	conf.AgentX.Address = strings.TrimSpace(v.GetString("AgentX.Address"))
	conf.AgentX.Net = strings.TrimSpace(v.GetString("AgentX.Net"))
	conf.AgentX.Timeout = v.GetDuration("AgentX.Timeout") * time.Second
	conf.AgentX.ReconnectInterval = v.GetDuration("AgentX.ReconnectInterval") * time.Second

	conf.ZabbixAPI.Endpoint = strings.TrimSpace(v.GetString("ZabbixAPI.Endpoint"))
	conf.ZabbixAPI.User = strings.TrimSpace(v.GetString("ZabbixAPI.User"))
	conf.ZabbixAPI.Secret = v.GetString("ZabbixAPI.Secret")
	conf.ZabbixAPI.Timeout = v.GetDuration("ZabbixAPI.Timeout") * time.Second

	conf.SNMP.ProblemBaseOID = strings.TrimSpace(v.GetString("SNMP.ProblemBaseOID"))
	conf.SNMP.ProblemMinSeverity = v.GetInt("SNMP.ProblemMinSeverity")
	conf.SNMP.ProblemHideAck = v.GetBool("SNMP.ProblemHideAck")
	conf.SNMP.ProblemTagFilter = strings.TrimSpace(v.GetString("SNMP.ProblemTagFilter"))
	conf.SNMP.TrapTimer = v.GetInt("SNMP.TrapTimer")
	conf.SNMP.BaseOID = strings.TrimSpace(v.GetString("SNMP.BaseOID"))
	conf.SNMP.TrapOID = strings.TrimSpace(v.GetString("SNMP.TrapOID"))

	OIDGroups := v.Get("SNMP.OIDGroups")
	conf.SNMP.OIDGroups = make(map[string]string, 0)

	if len(OIDGroups.([]interface{})) > 0 {
		for _, g := range OIDGroups.([]interface{}) {
			conf.SNMP.OIDGroups[strings.TrimSpace(g.(map[string]interface{})["Name"].(string))] = strings.TrimSpace(g.(map[string]interface{})["BaseOID"].(string))
		}
	}

	conf.SNMP.Hostgroups.CountUnknown = v.GetBool("SNMP.Hostgroups.CountUnknown")
	conf.SNMP.Hostgroups.BaseOID = strings.TrimSpace(v.GetString("SNMP.Hostgroups.BaseOID"))
	Hostgroups := v.Get("SNMP.Hostgroups.Groups")
	conf.SNMP.Hostgroups.Groups = make(map[string]string, 0)
	if len(Hostgroups.([]interface{})) > 0 {
		for _, g := range Hostgroups.([]interface{}) {
			conf.SNMP.Hostgroups.Groups[strings.TrimSpace(g.(map[string]interface{})["Name"].(string))] = strings.TrimSpace(g.(map[string]interface{})["OIDSuffix"].(string))
		}
	}

	err = validateConf(&conf)

	return &conf, err
}
