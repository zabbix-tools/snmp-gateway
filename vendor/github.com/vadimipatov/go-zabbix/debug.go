package zabbix

import (
	"log"
	"os"
)

// debug caches the value of environment variable SNMPGW_DEBUG_ZBX from program start.
var debug bool = (os.Getenv("SNMPGW_DEBUG_ZBX") == "1")

// dprintf prints formatted debug message to STDOUT if the SNMPGW_DEBUG_ZBX environment
// variable is set to "1".
func dprintf(format string, a ...interface{}) {
	if debug {
		log.Printf(format, a...)
	}
}
