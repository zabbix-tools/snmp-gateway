# Zabbix SNMP gateway
Zabbix SNMP gateway is an AgentX-extension for snmpd, which allows you to access Zabbix triggers data through the SNMP protocol.  
This service supports both SNMP polling and SNMP trapping.  
  
The implementation provided by [Zabbix SIA](zabbix.com).  

## Requirements
Zabbix >= 3.0
Go >= 1.8 (required just for build from sources)

## Installation
NOTE: Installation is given for RHEL/Centos. Some paths given may vary.
Assuming that Zabbix server, snmpd and Zabbix SNMP gateway all run on the same host.
    
### Configuring snmpd  
First you need to setup snmpd. If not installed, issue:  
`[user@localhost ~]# sudo yum install net-snmp net-snmp-utils`  
Once installed, append these lines to /etc/snmpd/snmpd.conf to enable agentx support:
```
# Enable the AgentX functionality:
master        agentx
# Define the timeout period (10 seconds) for an AgentX request. Default is 1 second.
agentXTimeout 10
# Do not retry failed requests
agentXRetries 0
# The default socket is the Unix Domain socket "/var/agentx/master"
# Another common alternative is tcp:localhost:705
# agentxsocket  tcp:localhost:705

# The trap2sink command defines a host that receives traps.
# trap2sink <sink-server>[:<port>] <community>
trap2sink 127.0.0.1 public
```

Might also require to remove the `-V systemonly` flag from `rocommunity`

```
rocommunity  public default
rocommunity6 public default -V systemonly
```

Change 127.0.0.1 to another address if it is required to catch SNMP traps by external system.  
And restart snmpd:  
`[user@localhost ~]# sudo service snmpd restart`  

### Configuring Zabbix SNMP gateway
Configure Zabbix SNMP gateway now.  
By default, the configuration file is searched in the current directory and /etc/zabbix/.
You can also specify the configuration file path manually using the "-c" flag:  
    
`[user@localhost ~]# zabbix-snmp-gateway -c ~/snmp-gateway.json`  
    
Supported formats of config file: json, toml, yaml and hcl.  
Here is example of configuration file with default values:

```json
{
  "MappingTable": {
    "RebuildInterval": 3600,
    "AllowManualRebuild": true                   // By SIGUSR2
  },
  "CacheLifeTime": 5,                            // Number of seconds how long query will be cached
  "HTTP": {
    "Net": "tcp",
    "Listen": "127.0.0.1:3000"
  },
  "InternalStatus": {                            // Self monitoring params
    "Enabled": true,
    "ShowConf": true,                            // Expose used configuration (without sensitive data)
    "Endpoint": "/debug/vars"
  },
  "AgentX": {
    "Net": "unix",                               // Which protocol to use: unix, tcp
    "Address": "/var/agentx/master",             // Path to unix-socket or tcp-address (e.g.: 127.0.0.1:705)
    "Timeout": 20,
    "ReconnectInterval": 5
  },
  "ZabbixAPI": {
    "Endpoint": "http://127.0.0.1/zabbix/api_jsonrpc.php",
    "User": "Admin",
    "Secret": "zabbix",
    "Timeout": 15
  },
  "SNMP": {
    "ProblemBaseOID": "1.3.6.1.4.1.3047.7.55",
    "ProblemMinSeverity": -1,
    "ProblemHideAck": false,
    "ProblemTagFilter": "problem",
    "BaseOID": "1.3.6.1.4.1.3043.7.55",
    "TrapOID": "1.3.6.1.4.1.3043.0",             // OID for traps
    "TrapTimer": 0,
    "OIDGroups": [
      {
        "Name": "ServiceA",
        "BaseOID": "1.3.6.1.4.1.3044.7.55"
      },
      {
        "Name": "ServiceB",
        "BaseOID": "1.3.6.1.4.1.3045.7.55"
      }
    ],
    "Hostgroups": {
      "CountUnknown": false,                     // Count triggers in an unknown state or not
      "BaseOID": "1.3.6.1.4.1.3046.7.55.10.11",
      "Groups": [
        {
          "Name": "Database Servers",
          "OIDSuffix": "1"
        },

        {
          "Name": "Frontend Cluster",
          "OIDSuffix": "2"
        }
      ]
    }
  }
}
```

Please check ZabbixAPI section and define proper OIDs.  

Moreover, all parameters can be passed through environment variables. To do this, add the prefix "SNMPGW_" to the parameter name, and write it in uppercase. Addressing to subsections is possible through the symbol "_".

**Example:**  

```sh
[user@localhost ~]# env SNMPGW_ZABBIXAPI_USER=apiuser ./zabbix-snmp-gateway
```

If any parameter is not present in the resulting config, then the default value is used
(except for the SNMP.OIDGroups and SNMP.Hostgroups sections).

### Run Zabbix SNMP gateway  
Run Zabbix SNMP gateway:  
```sh
[user@localhost]$ ./zabbix-snmp-gateway
2018/04/27 18:58:32 Connected to Zabbix API v4.0.0
2018/04/27 18:58:32 Subtree 1.3.6.1.4.1.3045.7.55 binded to ServiceB
2018/04/27 18:58:32 Subtree 1.3.6.1.4.1.3044.7.55 binded to ServiceA
2018/04/27 18:58:32 Subtree 1.3.6.1.4.1.3043.7.55 binded to default
2018/04/27 18:58:32 Subtree 1.3.6.1.4.1.3046.7.55.10.11 binded to hostgroups
2018/04/27 18:58:32 Connected to snmpd
2018/04/27 18:58:33 Mapping table builded (9 elements cached)
2018/04/27 18:58:33 HTTP server started on 0:3000
2018/04/27 18:58:33 Ready
```
We can ignore `Mapping table build failed: no data for caching` for now as we need to configure triggers in Zabbix first.  

### Usage

Triggers can be retrieved by OID that are concatenation of  
1. `OIDGroup + OIDSuffix` or  
2. `BaseOID + OIDSuffix` if `OIDGroup` is not defined in the trigger or configuration file.
3. `ProblemBaseOID+ TriggerID` for problem trigger retrieval.

*For example:*  
BaseOID=1.3.6.1.4 # value from Zabbix SNMP daemon configuration file  
Trigger tags: OIDSuffix:3  
In this case all information for this trigger will be available under OID=1.3.6.1.4.X.3, where X — trigger fields.

*Another example:*  
BaseOID=1.3.6.1.4 # value from Zabbix SNMP daemon configuration file  
OIDGroup=ServiceA,1.7.8.9.10 # value from Zabbix SNMP daemon configuration file, comma  
separated service name and base OID  

TriggerA tags: OIDSuffix:3  
TriggerB tags: OIDSuffix:1 OIDGroup:ServiceA  
TriggerC tags: OIDSuffix:2 OIDGroup:ServiceB  

In this case all information for these triggers will be available as defined below:  
TriggerA OID: 1.3.6.1.4.X.3 # default BaseOID is used  
TriggerB OID: 1.7.8.9.10.X.1 # OID of ServiceA is used  
TriggerC OID: 1.3.6.1.4.X.2 # default BaseOID is used because OIDGroup for ServiceB does not exist  

*Problem Trigger example.*
ProblemBaseOID=1.3.6.1.4 # value from Zabbix SNMP daemon configuration file  
Trigger tags: If trigger has a tag by which to find it must be defined in ProblemTagFilter, in config.   
In this case all information for this trigger will be available under OID=1.3.6.1.4.X.Y, where X — trigger fields and Y is trigger ID.

So before you can start polling triggers with SNMP, you need to tag each trigger accordingly.  
Using Zabbix web interface add additional tags (`OIDSuffix`, `OIDGroup`) with values to **host-level** triggers with values, like so  
`OIDSuffix`: 1  

For Problem triggers if ProblemTagFilter is not set in config, all problem triggers will be available.

Once your test trigger is set up, your requests should return valid data (wait for cache reload or restart SNMP gateway to speed up):

```sh
[user@localhost ~]# snmpget -v2c -c public 127.1 1.3.6.1.4.1.3043.7.55.2.3
SNMPv2-SMI::enterprises.3043.7.55.2.3 = INTEGER: 15247
```

```sh
[user@localhost ~]# snmpwalk -v2c -c public 127.0.0.1 1.3.6.1.4.1.3043.7.55
SNMPv2-SMI::enterprises.3043.7.55.1.1 = INTEGER: 1
SNMPv2-SMI::enterprises.3043.7.55.1.3 = INTEGER: 3
SNMPv2-SMI::enterprises.3043.7.55.1.4 = INTEGER: 4
SNMPv2-SMI::enterprises.3043.7.55.1.5 = INTEGER: 5
SNMPv2-SMI::enterprises.3043.7.55.1.6 = INTEGER: 6
SNMPv2-SMI::enterprises.3043.7.55.1.10 = INTEGER: 10
SNMPv2-SMI::enterprises.3043.7.55.2.1 = INTEGER: 15367
SNMPv2-SMI::enterprises.3043.7.55.2.3 = INTEGER: 15247
SNMPv2-SMI::enterprises.3043.7.55.2.4 = INTEGER: 15365
SNMPv2-SMI::enterprises.3043.7.55.2.5 = INTEGER: 15366
SNMPv2-SMI::enterprises.3043.7.55.2.6 = INTEGER: 13493
SNMPv2-SMI::enterprises.3043.7.55.2.10 = INTEGER: 13503
...
```

See 'Mapping object fields to OIDs' below how trigger attributes are mapped to OIDs.

## Host groups status polling
You can also poll Hostgroups status. In order to do so, provide proper mapping in Zabbix SNMP gateway configuration file:
```json
    "Hostgroups": {
      "BaseOID": "1.3.6.1.4.1.3046.7.55.10.11",
      "Groups": [
        {
          "Name": "Database Servers",
          "OIDSuffix": "1"
        },

        {
          "Name": "Frontend Cluster",
          "OIDSuffix": "2"
        }
      ]
    }
```
Each configured host group should exist, otherwise it will be skipped.

```sh
[user@localhost ~]# snmpwalk -v2c -c public 127.0.0.1 1.3.6.1.4.1.3046.7.55
SNMPv2-SMI::enterprises.3046.7.55.1.10.11.1 = INTEGER: 1
SNMPv2-SMI::enterprises.3046.7.55.1.10.11.2 = INTEGER: 2
SNMPv2-SMI::enterprises.3046.7.55.2.10.11.1 = INTEGER: 2
SNMPv2-SMI::enterprises.3046.7.55.2.10.11.2 = INTEGER: 4
SNMPv2-SMI::enterprises.3046.7.55.3.10.11.1 = STRING: "Database Servers"
SNMPv2-SMI::enterprises.3046.7.55.3.10.11.2 = STRING: "Frontend Cluster"
SNMPv2-SMI::enterprises.3046.7.55.4.10.11.1 = INTEGER: 0
SNMPv2-SMI::enterprises.3046.7.55.4.10.11.2 = INTEGER: 0
SNMPv2-SMI::enterprises.3046.7.55.5.10.11.1 = INTEGER: 0
SNMPv2-SMI::enterprises.3046.7.55.5.10.11.2 = INTEGER: 0
SNMPv2-SMI::enterprises.3046.7.55.6.10.11.1 = INTEGER: 2
SNMPv2-SMI::enterprises.3046.7.55.6.10.11.2 = INTEGER: 2
...
```

## Configuring SNMP traps
You can also configure to receive Zabbix trigger state changes as SNMP traps.
For that, you can create an Action in Zabbix that executes the following custom script:
 
`curl -X POST -d "triggerid={TRIGGER.ID}" {$SNMPGW_ADDR}/trap`

If you want the trap to be sent when the trigger is turned to the OK state, you can configure the same script as the Recovery operation.    

{$SNMPGW_ADDR} — a global macro that defines the zabbix-snmp-gateway address.    
**Example:**  

`{$SNMPGW_ADDR} = http://127.0.0.1:3000`

It is also possible to limit the rate at witch traps are created by setting the config parameter `TrapTimer` in milliseconds.
Is set, only one traps will only be created in the provided time intervale, and that of the highest severity, or latest, if
severity is tied.

Once you have configured everything correctly and the trigger moves to the PROBLEM state then in the snmptrapd (or in your external system) you would see your trap like this:    

```text
2018-04-27 19:11:24 0.0.0.0(via UDP: [192.168.33.1]:54321->[192.168.33.10]:162) TRAP, SNMP v1, community public
	.1.3.6.1.6.3.1.1.4.3.0.1.3.6.1.4.1.3043.7.55.2 Enterprise Specific Trap (1) Uptime: 0:05:09.66
	.1.3.6.1.4.1.3043.7.55.1.1 = INTEGER: 1	.1.3.6.1.4.1.3043.7.55.2.1 = INTEGER: 15367	.1.3.6.1.4.1.3043.7.55.3.1 = STRING: "{test2:test_item.last()}<>0"	.1.3.6.1.4.1.3043.7.55.4.1 = STRING: "test_trigger2"	.1.3.6.1.4.1.3043.7.55.5.1 = INTEGER: 3	.1.3.6.1.4.1.3043.7.55.6.1 = INTEGER: 0	.1.3.6.1.4.1.3043.7.55.7.1 = INTEGER: 1	.1.3.6.1.4.1.3043.7.55.8.1 = INTEGER: 0	.1.3.6.1.4.1.3043.7.55.9.1 = INTEGER: 1523458023
2018-04-27 19:11:24 <UNKNOWN> [UDP: [192.168.33.1]:53939->[192.168.33.10]:162]:
.1.3.6.1.2.1.1.3.0 = Timeticks: (30966) 0:05:09.66	.1.3.6.1.6.3.1.1.4.1.0 = OID: .1.3.6.1.6.3.1.1.4.3.0.1.3.6.1.4.1.3043.7.55.2.1	.1.3.6.1.4.1.3043.7.55.1.1 = INTEGER: 1	.1.3.6.1.4.1.3043.7.55.2.1 = INTEGER: 15367	.1.3.6.1.4.1.3043.7.55.3.1 = STRING: "{test2:test_item.last()}<>0"	.1.3.6.1.4.1.3043.7.55.4.1 = STRING: "test_trigger2"	.1.3.6.1.4.1.3043.7.55.5.1 = INTEGER: 3	.1.3.6.1.4.1.3043.7.55.6.1 = INTEGER: 0	.1.3.6.1.4.1.3043.7.55.7.1 = INTEGER: 1	.1.3.6.1.4.1.3043.7.55.8.1 = INTEGER: 0	.1.3.6.1.4.1.3043.7.55.9.1 = INTEGER: 1523458023
```

## Troubleshooting
You could set these environment variables in case you want to get extended debugging information:  
`SNMPGW_DEBUG_AGENTX=1` — logs communication between gateway and snmpd
`SNMPGW_DEBUG_HANDLERS=1` — logs handlers calls and source of data  
`SNMPGW_DEBUG_ZBX=1` — logs requests to Zabbix API  

Please don't forget that:
- You need to add at least `OIDSuffix` tag filled in triggers you want to poll.  
- Do not add OIDSuffix in templates. Add OIDsuffix tag at the host level as combination of OIDSuffix and BaseOID/OIDGroup should be unique.  

## License
The project is licensed under GPL 3.0 (see LICENSE.md file).


## Appendix 1  
### Mapping object fields to OIDs
**Triggers:**
```text
OIDSuffix   -> .1
TriggerID   -> .2
Expression  -> .3
Description -> .4
Priority    -> .5
State       -> .6
Status      -> .7
Value       -> .8
LastChange  -> .9
```

**Problem Triggers:**
```text
TriggerID → .2
Description → .4
Priority → .5
LastChange → .9
Hostname → .10
```

**Hostgroups:**
```text
OIDSuffix     -> .1
GroupID       -> .2
Name          -> .3
Disaster      -> .4
High          -> .5
Average       -> .6
Warning       -> .7
Information   -> .8
NotClassified -> .9
```

### HTTP API
Built-in HTTP server is used for handling traps requests and for accessing to internal statistics.

#### Get internal statistics
The service exposes some internal variables in the JSON format.
  
* **URL**  
  /debug/vars (endpoint can be changed in the config)

* **Method:**  
  `GET`
  
* **URL Params**  
  N/A

* **Success Response:**  
  **Code:** 200  
  **Content:**  
  ```json
  {
      "BuildTime": "1519398093",
      "Cache": {
        "HitRatio": "0.00",
        "Hits": 0,
        "Length": 0,
        "Misses": 0
      },    
      "Configuration": { 
        // There are many runtime configuration options (if InternalStatus.ShowConf enabled)
      },
      "Goroutines": 11,
      "MappingTable": {
          "BuildTime": "1519398836",
          "Elements": 6
      },
      "Uptime": 430646649000,
      "Version": "1.0.0",
      "cmdline": [
          "/usr/sbin/zabbix-snmp-gateway"
      ],
      "memstats": {
          "Alloc": 3415840,
          "TotalAlloc": 299286584,
          "Sys": 11376888,
          "Lookups": 18,
          "Mallocs": 5212549,
          "Frees": 5179351,
          "HeapAlloc": 3415840,
          "HeapSys": 6717440,
          "HeapIdle": 2408448,
          "HeapInuse": 4308992,
          "HeapReleased": 0,
          "HeapObjects": 33198,
          "StackInuse": 622592,
          "StackSys": 622592,
          "MSpanInuse": 50920,
          "MSpanSys": 65536,
          "MCacheInuse": 13888,
          "MCacheSys": 16384,
          "BuckHashSys": 1460242,
          "GCSys": 438272,
          "OtherSys": 2056422,
          "NextGC": 4194304,
          "LastGC": 1519399175850116000,
          "PauseTotalNs": 6083342,
          "PauseNs": [
              133040,
              ...
          ],
          "PauseEnd": [
              ...
          ],
          "NumGC": 105,
          "NumForcedGC": 0,
          "GCCPUFraction": -3.3095529203223486e-8,
          "EnableGC": true,
          "DebugGC": false,
          "BySize": [
              {
                  "Size": 0,
                  "Mallocs": 0,
                  "Frees": 0
              },
              ...
          ]
      }
  }
  ```
 
* **Error Response:**  
  You should get 404 if InternalStatus.Enabled == false  
  **Code:** 404 NOT FOUND

* **Sample Call:**  
  ```sh
  curl --request GET \
    --url http://127.0.0.1:3000/debug/vars 
  ```
 
* **URL**  
  /debug/maptable

* **Method:**  
  `GET`
  
* **URL Params**  
  N/A

* **Success Response:**  
  **Code:** 200  
  **Content:**  
  ```json
{
    "BuildTime": "2018-04-27T18:32:14.286466+03:00",
    "Data": {
        "default": {
            "1.3.6.1.4.1.3043.7.55.X.3": {
                "TriggerID": "15247",
                "Host": [
                    {
                        "HostID": "10255",
                        "Name": "mysql-slave"
                    }
                ],
                "Description": "mysql.performance.slow_queries"
            },
            "1.3.6.1.4.1.3043.7.55.X.10": {
                "TriggerID": "13503",
                "Host": [
                    {
                        "HostID": "10084",
                        "Name": "Zabbix server"
                    }
                ],
                "Description": "/etc/passwd has been changed on Zabbix server"
            },
            ...
        },
        "ServiceA": {
            "1.3.6.1.4.1.3044.7.55.X.9": {
                "TriggerID": "15246",
                "Host": [
                    {
                        "HostID": "10254",
                        "Name": "mysql-master"
                    }
                ],
                "Description": "mysql.performance.slow_queries"
            }
        },
        ...
    },
    "Elements": 9
}
  ```
 
* **Sample Call:**  
  ```sh
  curl --request GET \
    --url http://127.0.0.1:3000/debug/maptable 
  ``` 
 
#### Trap trigger
  
* **URL**  
  /trap

* **Method:**  
  `POST`
  
*  **URL Params**    
   **Required:**  
   `triggerid=[integer]`

* **Success Response:**  
  **Code:** 200
 
* **Error Response:**    
  **Code:** 404 NOT FOUND

* **Sample Call:** 
  ```sh
  curl --request POST \
    -d "triggerid=15000 \
    --url http://127.0.0.1:3000/trap
  ```   

### Mapping table
Mapping table — is a cache that stores mappings of OIDs to triggers. 
It rebuilding automatically by interval defined in configuration parameter "MappingTable.RebuildInterval". Default: 1h.  
You could also manually rebuild mapping table at any time by sending SIGUSR2 signal to zabbix-snmp-gateway process.
`[user@localhost ~]# killall -USR2 zabbix-snmp-gateway`

You could get content of mapping table by send GET request to /debug/maptable endpoint.  
