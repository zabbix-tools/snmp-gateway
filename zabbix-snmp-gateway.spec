Name:		zabbix-snmp-gateway
Version:	%{version}
Release:	%{release}%{?dist}
Summary:	zabbix-snmp-gateway
Group:		System Environment
License:	GPL
Source:		zabbix-snmp-gateway.tar.gz
Vendor:		Zabbix
AutoReqProv:	no

%description
Zabbix SNMP Gateway is an AgentX-extension for snmpd, which allows you to access Zabbix triggers data through the SNMP protocol.
This service supports both SNMP polling and SNMP trapping.


%prep
%setup -q -n %{name}
echo %{buildroot}


%install
mkdir -p $RPM_BUILD_ROOT/usr/sbin/
mv zabbix-snmp-gateway.linux.%{arch} $RPM_BUILD_ROOT/usr/sbin/zabbix-snmp-gateway
cp -r etc/ $RPM_BUILD_ROOT%{_sysconfdir}
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/zabbix
cp snmp-gateway.json $RPM_BUILD_ROOT%{_sysconfdir}/zabbix/

%files
%defattr(-, root, root, 0755)
/usr/sbin/zabbix-snmp-gateway
%config(noreplace) /etc/zabbix/snmp-gateway.json
%config(noreplace) /etc/logrotate.d/zabbix-snmp-gateway
%config(noreplace) /etc/rsyslog.d/zabbix-snmp-gateway.conf
%config(noreplace) /etc/systemd/system/zabbix-snmp-gateway.service

%post
# Initial and update cases:
    mkdir -p /var/log/zabbix
    mkdir -p /run/zabbix/
    mkdir -p /etc/zabbix/
    systemctl restart rsyslog
    systemctl --system daemon-reload

# Initial installation
if [ "$1" = "1" ]; then
    systemctl enable zabbix-snmp-gateway
	echo "Please configure service in accordance with the documentation"
fi

%preun
if [ "$1" = "0" ]; then
    # Uninstall
    systemctl stop zabbix-snmp-gateway
    systemctl disable zabbix-snmp-gateway
elif [ "$1" = "1" ]; then
    # Update
    echo "Please restart zabbix-snmp-gateway manually"
fi


%postun
if [ "$1" = "0" ]; then
    # Uninstall
    systemctl --system daemon-reload
fi


%clean
rm -rf $RPM_BUILD_ROOT%