/*
** Zabbix
** Copyright (C) 2001-2018 Zabbix SIA
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, version 3.
**
** This program is distributed in the hope that it will be useful, but
** WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

package main

import (
	"expvar"
	"net/http"
	"runtime"
	"strconv"
	"time"
)

// getNumGoroutines is an expvar.Func compliant wrapper for runtime.NumGoroutine function
func getNumGoroutines() interface{} {
	return runtime.NumGoroutine()
}

// getUptime is an expvar.Func compliant wrapper for uptime info
func getUptime() interface{} {
	return int64(time.Since(startTime))
}

// getConfiguration is an expvar.Func compliant wrapper for configuration info
func getConfiguration() interface{} {
	return conf
}

// getCacheStat is an expvar.Func compliant wrapper for cache statistics
func getCacheStat() interface{} {
	tc_hits, _ := transactionCache.GetCacheStat()
	qc_hits, qc_misses, qc_len := queryCache.GetCacheStat()
	hits := tc_hits + qc_hits
	hitRatio := 0.
	if (hits + qc_misses) > 0 {
		hitRatio = float64(hits) / float64(hits+qc_misses) * 100.
	}
	return map[string]interface{}{
		"Hits":     hits,
		"Misses":   qc_misses,
		"HitRatio": strconv.FormatFloat(hitRatio, 'f', 2, 64),
		"Length":   qc_len,
	}
}

// getMappingTableStat TODO: add description
func getMappingTableStat() interface{} {
	return map[string]interface{}{
		"Elements":  mapTable.GetElementsCount(),
		"BuildTime": strconv.Itoa(int(mapTable.GetBuildTime())),
	}
}

// setupMonitoring TODO: add description
func setupMonitoring() http.Handler {
	expvar.NewString("Version").Set(VERSION)
	expvar.NewString("BuildTime").Set(BUILD_TIME)
	expvar.Publish("Goroutines", expvar.Func(getNumGoroutines))
	expvar.Publish("Uptime", expvar.Func(getUptime))
	expvar.Publish("Cache", expvar.Func(getCacheStat))
	expvar.Publish("MappingTable", expvar.Func(getMappingTableStat))
	if conf.InternalStatus.ShowConf {
		expvar.Publish("Configuration", expvar.Func(getConfiguration))
	}
	return expvar.Handler()
}
